party-photo-share(仮)
======================

[![wercker status](https://app.wercker.com/status/96d44a2c4e815cdbf511f0c314d4e212/s "wercker status")](https://app.wercker.com/project/bykey/96d44a2c4e815cdbf511f0c314d4e212)

[Wiki](https://bitbucket.org/mid0111/party-photo-share/wiki/Home)


## 開発環境構築

```bash
$ git clone git@bitbucket.org:mid0111/party-photo-share.git && cd party-photo-share
$ npm install && bower install

$ cp config/auth.sample.js config/auth.js
$ vi config/auth.js

$ docker run -p 27017:27017 --name mongodb -d mongo
$ ./node_modules/gulp/bin/gulp.js
```

### MongoDB Client

```bash
$ docker run -it --rm --link mongodb:mongodb mongo bash -c 'mongo --host mongodb'
```

## Nginx(http_auth_request_module)を通した検証をしたい場合

```bash
$ docker build -t nginx-auth ./resources/nginx/
$ gulp build && docker run --name node -d -p 3000:3000 -v `pwd`:/data node node ./app.js
$ docker run --name nginx -it --rm -p 80:80 -v `pwd`/resources/nginx/conf.d:/etc/nginx/conf.d:ro -v `pwd`/build:/var/www/html --link node:nodejs nginx-auth /bin/bash
```

## テスト

```bash
$ gulp test
```

## デプロイ

* Create inventory file for ansible and specify target hostname.
```bash
$ cp resources/ansible/hosts.sample resources/ansible/hosts
$ vi resources/ansible/hosts
```

* Deploy
```bash
$ docker run -it --rm --name ansible -v `pwd`/resources/ansible:/tmp/ansible -v `pwd`/config:/tmp/secret -v {path_for_pem_file}:/root/pem ansible/ubuntu14.04-ansible:stable ansible-playbook /tmp/ansible/site.yml --private-key="/root/pem/{pem_file_name}" -i /tmp/ansible/hosts
```
