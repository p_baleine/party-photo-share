process.env.PORT = '3001';
process.env.BLOB_PATH = './test/uploads/';
process.env.MONGO_HOST = process.env.WERCKER_MONGODB_HOST || '192.168.59.103';

module.exports = app = require('../app');

