var request = require('supertest');
var express = require('express');
var fs = require('fs');
var expect = require('chai').expect;

var blobPath = __dirname + '/uploads/';

var app = require('./app');
var passportStub = require('passport-stub');

describe('GET /photos', function() {

  before(function() {
    passportStub.install(app);
    passportStub.login({username: 'john.doe'});
  });

  it('ファイルの一覧画面が表示されること', function(done) {
    request(app)
      .get('/photos')
      .expect(200)
      .expect('Content-Type', 'text/html; charset=utf-8')
      .end(function(err, res){
        if (err) throw err;
        expect(res.body.photos).to.be.an('undefined');
        done();
      });
  });

  it('クエリつきの場合ファイルの一覧が返却されること', function(done) {
    request(app)
      .get('/photos?show=0')
      .expect(200)
      .expect('Content-Type', 'application/json; charset=utf-8')
      .end(function(err, res){
        if (err) throw err;
        expect(res.body.photos).to.be.instanceof(Array);
        done();
      });
  });

});
