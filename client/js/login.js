var React = require('react');
var MinLogo = require('./components/logo.js').MinLogo;

React.render(<MinLogo />, document.getElementById('title-frame-min'));

var logInButton = document.getElementById('login-button');

logInButton.onclick = function() {

  var email = document.getElementById('login-email').value;
  var password = document.getElementById('login-password').value;

  if(!email || !password) {
    var message = document.getElementById('login-warn');
    message.innerHTML = '必須項目が入力されていません。';
    message.hidden = false;

    return false;
  }

  return true;
};
