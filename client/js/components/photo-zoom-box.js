var React = require('react');
var request = require('request');
var PhotoZoom = require('./photo-zoom');
var NavNextPhoto = require('./nav-photo');
var PhotoMetaInf = require('./photo-meta-inf');

module.exports = PhotoZoomBox = React.createClass({
  propTypes: {
    _id: React.PropTypes.string.isRequired,
    name: React.PropTypes.string.isRequired,
    originalname: React.PropTypes.string.isRequired,
    width: React.PropTypes.number.isRequired,
    height: React.PropTypes.number.isRequired,
    author: React.PropTypes.object.isRequired,
    starCount: React.PropTypes.number.isRequired,
    stars: React.PropTypes.array.isRequired,
    enableNavLeft: React.PropTypes.bool.isRequired,
    enableNavRight: React.PropTypes.bool.isRequired,
    handleOnPrevZoom: React.PropTypes.func.isRequired,
    handleOnNextZoom: React.PropTypes.func.isRequired,
    handleOffZoom: React.PropTypes.func.isRequired,
    handleOnStar: React.PropTypes.func.isRequired
  },

  getInitialState: function() {
    return {
      isStared: this.isStared(this.props),
      starCount: this.props.starCount
    };
  },

  componentWillReceiveProps: function(nextProps) {
    this.setState({
      isStared: this.isStared(nextProps),
      starCount: nextProps.starCount
    });
  },

  isStared: function(props) {
    return this.props.stars.indexOf(props._id) >= 0;
  },

  _handleNavPrevClick: function() {
    this.props.handleOnPrevZoom();
  },

  _handleNavNextClick: function() {
    this.props.handleOnNextZoom();
  },

  _handleOffZoom: function() {
    this.props.handleOffZoom();
  },

  handleOnStar: function() {
    updateStar(this.props._id, this.state.isStared)
      .then(function() {
        var starCount = this.state.starCount + 1;
        if(this.state.isStared) {
          starCount = this.state.starCount - 1;
        }
        this.setState({
          isStared: !this.state.isStared,
          starCount: starCount
        });
        this.props.handleOnStar(this.props, starCount);
      }.bind(this));
  },

  getOriginalUrl: function() {
    return 'img/uploads/org/' + this.props.name;
  },

  getUrl: function() {
    return '/img/uploads/m/' + this.props.name;
  },

  render: function() {
    var downloadUrl = window.location.origin + '/' + this.getOriginalUrl();
    return (
        <div>
        <div className="photo-zoom-frame">
        <NavPhoto className="page-navigator-left" direction='next' handleClick={this._handleNavNextClick} enable={this.props.enableNavLeft} />
        <NavPhoto className="page-navigator-right" direction='prev' handleClick={this._handleNavPrevClick} enable={this.props.enableNavRight}/>

        <PhotoZoom
      url={this.getUrl()}
      originalname={this.props.originalname}
      width={this.props.width}
      height={this.props.height}
        />

        </div>

        <PhotoMetaInf
      author={this.props.author.name}
      starCount={this.state.starCount}
      isStared={this.state.isStared}
      handleOnStar={this.handleOnStar}
        />

        <div>
        <a href={downloadUrl} title="Download this photo" download={this.props.originalname}>
        <button id="button-download" className="pure-button button button-zoom">ダウンロード</button>
        </a>
        </div>
        </div>
    );
  }
});

function updateStar(id, isStared) {
  var path = '/photos/star/countup';
  if(isStared) {
    path = '/photos/star/countdown';
  }
  return new Promise(function(onFulfilled, onRejected) {
    request.put({
      uri: window.location.origin + path + '?id=' + id
    }, function(error, res, body) {
      if(error) {
        return onRejected(error);
      }
      return onFulfilled(body);
    });
  });
}
